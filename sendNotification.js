var sendNotification = function (data, callback) {
  var headers = {
    "Content-Type": "application/json; charset=utf-8"
  };

  var options = {
    host: "onesignal.com",
    port: 443,
    path: "/api/v1/notifications",
    method: "POST",
    headers: headers,
    timeout: 4000
  };

  var https = require('https');
  log('calling one signal', options);
  var req = https.request(options, function (res) {
    res.on('data', function (data) {
      console.log(data)
      //console.lole.log("Response:");
      //console.lole.log(JSON.parse(data));
      log('response from one signal', data);
      callback();
    });
  });

  req.on('error', function (e) {
    //console.lole.log("ERROR:");
    //console.lole.log(e);
    log('error from one signal', data);
    callback();
  });

  req.write(JSON.stringify(data));
  req.end();
};

function log(...log) {
  console.log(log.join(' '));
}

module.exports = sendNotification;