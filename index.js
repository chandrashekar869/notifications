const config = require('config');
const mongoose = require('mongoose');
const compare = require('./pollutantCompare');
const readings = require('./models/readings/readings.controller');

const mongoDB = config.mongooseURL;

mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;

const db = mongoose.connection;

db.on('error', () => {
    console.error.bind(console, 'MongoDB connection error:');
});

function destroy() {
    db.close();
}

// setInterval(() => {
//     readings.addNewReadings();
// }, 3000);

exports.handler = () => {
    readings.getReadings()
        .exec((err, result) => {
            if (err) throw err;
            else compare.checkThresholds(result, destroy);
        });
};
// exports.handler()