const async = require('async');
const request = require('request');
const moment = require('moment');
const user = require('./models/users/users.controllers');
const alert = require('./models/alerts/alerts.controller');
const notificationHelper = require('./sendNotification');
const thresholds = {
    // PM25: 60,
    // PM1: 41,
    // PM10: 101,
    // CO2: 351,
    // TVOC: 0.21,
    // HCHO: 0.03
    PM25: 36,
    PM1: 25,
    PM10: 51,
    CO2: 1081,
    TVOC: 0.21,
    HCHO: 0.03
};
let destroy;

function log(...log) {
    // console.log(log.join(' '));
}

function checkThresholds(data, destroyConnection) {
    // //console.log(data);
    log('started', data.length);
    destroy = destroyConnection;
    async.map(data, (device, deviceCb) => {
        log('device', device.recentData.devId);
        const keys = Object.keys(device);
        async.map(keys, (key, keyCb) => {
            const pollutant = key.replace('average', '');
            if(pollutant === 'HCHO') {
                device.recentData.HCHO = device.recentData.HCHO/1000;
            }
            const value = device[key]
            const thresholdValue = thresholds[pollutant];
            log('device', pollutant);
            // updateOldOnes(device, pollutant);
            switch (true) {
                case (!value || !thresholdValue): invalid(key, value, keyCb); break;
                case (value > thresholdValue): wentHigh(device, pollutant, value, thresholdValue, keyCb); break;
                case (value < thresholdValue): wentLow(device, pollutant, value, thresholdValue, keyCb); break;
                default: log('default'); keyCb();
            }
        }, (err) => {
            if (err) {
                log('error for device map async');
                destroy();
                throw err;
            }
            else { log('keys done device cb calling'); deviceCb(); }
        });
    }, (err) => {
        if (err) {
            log('error device async map');
            destroy();
            throw err;
        }
        else {
            log('Done devices');
            //console.log('Done');
            destroy();
        }
    });
}

// function updateOldOnes(device, pollutant) {
//     getAlertsTrue(device, pollutant)
//     .then(data => {
//         data = data[0];
//         if(data && data.status !== undefined) {
//             data.updatedAt = moment.utc().toDate();
//             data.recentData = {
//                 _id : device.recentData._id,
//                 devId : device.recentData.devId,
//                 name : device.recentData.name,
//                 [pollutant]: device.recentData[pollutant]
//             };
//             data.userId = device.recentData.userId;
//             data.max = device.recentData[pollutant] > data.max ? device.recentData[pollutant] : data.max;
//             data.min = device.recentData[pollutant] < data.min ? device.recentData[pollutant] : data.min;
//             data.markModified('max');
//             data.markModified('min');
//             data.markModified('userId');
//             data.markModified('updatedAt');
//             data.markModified('recentData');
//             data.save((err, result) => {
//                 if(err) {
//                     destroy();
//                     throw err;
//                 }
//                 else //console.log(result);
//             });
//         }
//     })
//     .catch(err => {
//         console.error(err);
//     })
// }

function getAlerts(device, pollutant) {
    //console.log(device);
    return new Promise((res, rej) => {
        alert.get({
            $and: [
                { devId: device._id },
                { status: false },
                { pollutant: pollutant }
            ]
        }).exec((err, result) => {
            if (err) rej(err);
            else {
                res(result);
            }
        })
    });
}


function getAlertsTrue(device, pollutant) {
    //console.log(device);
    return new Promise((res, rej) => {
        alert.get({
            $and: [
                { devId: device._id },
                { status: true },
                { pollutant: pollutant }
            ]
        }).exec((err, result) => {
            if (err) rej(err);
            else res(result);
        })
    });
}


function wentHigh(device, pollutant, value, thresholdValue, callback) {
    log('went high for device ', device.recentData._id);
    getAlerts(device, pollutant)
        .then(data => {
            data = data[0];
            if (data && data.status !== undefined) {
                log('got alerts with status', device.recentData._id);
                data.updated = true;
                data.updatedAt = moment.utc().toDate();
                data.recentData = {
                    _id: device.recentData._id,
                    devId: device.recentData.devId,
                    name: device.recentData.name,
                    [pollutant]: device.recentData[pollutant]
                };
                data.max = device.recentData[pollutant] > data.max ? device.recentData[pollutant] : data.max;
                data.min = device.recentData[pollutant] < data.min ? device.recentData[pollutant] : data.min;
                data.userId = device.recentData.userId;
                data.markModified('updated');
                data.markModified('max');
                data.markModified('min');
                data.markModified('userId');
                data.markModified('updatedAt');
                data.markModified('recentData');
                data.save((err, result) => {
                    if (err) {
                        log('err saving alerts with status', device.recentData._id);
                        destroy();
                        throw err;
                    }
                    else log('success saving alerts with status', device.recentData._id);//console.log(result);
                    callback();
                });
            } else {
                log('no alerts with status', device.recentData._id);
                const deviceData = {
                    _id: device.recentData._id,
                    devId: device.recentData.devId,
                    name: device.recentData.name,
                    [pollutant]: device.recentData[pollutant]
                };

                alert.insert({
                    devId: device._id,
                    pollutant: pollutant,
                    status: false,
                    updated: false,
                    max: device.recentData[pollutant],
                    min: device.recentData[pollutant],
                    threshold: thresholdValue,
                    createdAt: moment.utc().toDate(),
                    updatedAt: moment.utc().toDate(),
                    userId: device.recentData.userId,
                    originalData: deviceData,
                    recentData: deviceData
                }).then(() => {
                    log('success inserting alerts with status', device.recentData._id);
                    sendNotification(device, pollutant, true, callback);
                }).catch((err) => {
                    log('err inserting alerts with status', device.recentData._id);
                    destroy();
                    throw err;
                })
            }
        })
        .catch((err) => {
            log('error getting alerts with status', device.recentData._id);
            destroy();
            throw err;
        });
    //If exists a alert
    //  status:false 
    //      AND
    //  Same pollutant
    //      THEN
    //          Update last updated and updated true and means that notification is still open dont send any notification
    //Else alert doesnt exist
    //  status:false
    //      AND
    //  Same pollutant
    //      THEN
    //          Send a notification
    //          Push to alerts with status false and updated false 
}

function wentLow(device, pollutant, value, thresholdValue, callback) {
    //console.log('went low');
    log('went low', device.recentData._id);
    getAlerts(device, pollutant)
        .then(data => {
            data = data[0];
            if (data && data.status !== undefined) {
                log('went low got alerts with status', device.recentData._id);
                data.updated = true;
                data.status = true;
                data.updatedAt = moment.utc().toDate();
                data.recentData = {
                    _id: device.recentData._id,
                    name: device.recentData.name,
                    [pollutant]: device.recentData[pollutant]
                };
                data.userId = device.recentData.userId;
                data.max = device.recentData[pollutant] > data.max ? device.recentData[pollutant] : data.max;
                data.min = device.recentData[pollutant] < data.min ? device.recentData[pollutant] : data.min;
                data.markModified('updated');
                data.markModified('userId');
                data.markModified('max');
                data.markModified('min');
                data.markModified('status');
                data.markModified('updatedAt');
                data.markModified('recentData');
                data.save((err, result) => {
                    if (err) {
                        log('error saving low  alerts with status', device.recentData._id);
                        destroy();
                        throw err;
                    } else {
                        log('success saving low  alerts with status', device.recentData._id);
                        sendNotification(device, pollutant, false, callback);
                    }
                    // callback();
                });
            } else {
                log('got no low  alerts with status', device.recentData._id);
                callback();
                // Send a notification with safe theme
                // alert.insert({
                //         devId: device._id,
                //         pollutant: pollutant,
                //         status: true,
                //         updated: false,
                //         createdAt: moment.utc().toDate(),
                //         updatedAt: moment.utc().toDate(),
                //         originalData: {
                //             _id : device.recentData._id,
                //             devId : device.recentData.devId,
                //             name : device.recentData.name,
                //             [pollutant]: device.recentData[pollutant]
                //         }
                // })
                // .then(data => {
                //     //console.log(data);
                // })
                // .catch((err) => {
                //     if(err) throw err;
                // }) 
            }
        })
        .catch((err) => {
            log('error getting low  alerts', device.recentData._id);
            destroy();
            throw err;
        });
    //If exists a alert
    //  status:false
    //      AND
    //  Same pollutant
    //      THEN
    //          Close status to true and updated to true and last Update
    //ELSE 
    //  send notification 
    //  Push to alerts with status true and updated true
}

function invalid(key, value, cb) {
    log('invalid');
    //console.log('Invalid');
    cb();
}

async function sendNotification(device, pollutant, isHigh, callback) {
    const userDetails = await user.getById(device.recentData.userId) || null;
    if (!userDetails || !userDetails.company) {
        log('couldnt send notification to user', device.recentData.userId);
        callback();
        return;
    }
    log('calling notification', device.recentData.userId);
    const deviceDetails = device.recentData.name.split('_');
    const deviceName = deviceDetails[deviceDetails.length - 2];
    const deviceNumber = deviceDetails[deviceDetails.length - 1];
    var message = {
        app_id: "f6a37030-0bad-4bcf-aa08-d3c1ceba51c6",
        headings: { "en": isHigh ? `Spike in ${pollutant} levels` : `${pollutant} back to safe levels` },
        contents: { "en": `${pollutant} is currently ${Number(device.recentData[pollutant]).toFixed(2)} at ${deviceDetails[1]} ${deviceName}` },
        include_player_ids: [userDetails.b2bOneSignalId]
    };
    if(userDetails.toJSON().notificationEmails !== undefined) {
        async.map(userDetails.toJSON().notificationEmails, (email, cb) => {
            sendMail({
                "from": "alerts@getambee.com","to": email,"subject": `Alert - ${deviceDetails[1]} ${deviceName} ${deviceNumber}` ,"text": message.headings.en+'\n'+ message.contents.en
            }, cb);
        }, (err) => {
            if(err) callback();
            else {
                if (userDetails.b2bOneSignalId) 
                    notificationHelper(message, callback);
                else callback();
            }
        });
    } else {
        if (userDetails.b2bOneSignalId) 
            notificationHelper(message, callback);
        else callback();
    }
}

function sendMail(body, cb) {
    var options = { method: 'POST',
  url: 'https://p6md6eyc8i.execute-api.us-east-1.amazonaws.com/dev/sendMail',
  headers: 
   { 
     'Content-Type': 'application/json' 
    },
  body: body,
  json: true };

request(options, function (error, response, body) {
  if (error) {cb();}
  else {cb();}
});
}

module.exports = {
    checkThresholds
}