let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let userSchema = new Schema({
  name: { type: String, required: true },
  username: { type: String },
  email: { type: String, required: true },
  phone: { type: String },
  password: { type: String, required: true },
  address: { type: String },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  lastLogin: { type: Date, default: Date.now },
  company: { type: String, default: null },
  jobTitle: { type: String },
  b2bOneSignalId: { type: String },
  roleId: { type: Number, default: 3 },
  lat: { type: String },
  lng: { type: String }
});

const User = mongoose.model('user', userSchema);
module.exports = User;