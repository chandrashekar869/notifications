const user = require('./users.model');

function getById(condtion) {
    return new Promise((resolve, reject) => {
        user.findById(condtion, (err, user) => {
            if (err) reject(err);
            else {
                resolve(user);
            }
        });
    });
}

module.exports = {
    getById
};