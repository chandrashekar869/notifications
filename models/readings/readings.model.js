let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let ObjectId = mongoose.Schema.Types.ObjectId;
// Schema definition
let ReadingsSchema = new Schema(
  {
    deviceId: { type: ObjectId },
    devId: { type: String },
    userId: { type: ObjectId },
    name: { type: String },
    mac: { type: String },
    PM25: { type: Number },
    PM1: { type: Number },
    PM10: { type: Number },
    CO2: { type: Number },
    TVOC: { type: Number },
    HCHO: { type: Number },
    SLOPE: { type: Number },
    ssid: { type: String },
    systemTick: { type: Number },
    lastUpdate: { type: String },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date },
  }
);
// Sets the createdAt parameter equal to the current time
ReadingsSchema.pre('save', next => {
  now = new Date();
  if (!this.createdAt) {
    this.createdAt = now;
  }
  next();
});

// Exports the Schema for use elsewhere.
module.exports = mongoose.model('readings', ReadingsSchema);