const readings = require('./readings.model');
const moment = require('moment');

const dummyReadings = [{
    devId: "D1-bbabca75c1b0421e976d6d7ab1b39bec",
    name: "Device 1",
    mac: "",
    PM25: 30,
    PM1: 10,
    PM10: 50,
    CO2: 700,
    TVOC: 0.25,
    HCHO: 0.6,
    SLOPE: 0,
    ssid: "",
    systemTick: 0,
    lastUpdate: new Date().toISOString(),
    createdAt: new Date(),
    updatedAt: new Date()
}, {
    devId: "D1-657373a1e14ef08b3dc854a76b90adb6",
    name: "Device 2",
    mac: "",
    PM25: 40,
    PM1: 11,
    PM10: 60,
    CO2: 300,
    TVOC: 0.15,
    HCHO: 0.9,
    SLOPE: 0,
    ssid: "",
    systemTick: 0,
    lastUpdate: new Date().toISOString(),
    createdAt: new Date(),
    updatedAt: new Date()
}, {
    devId: "D1-bbabca75c1b0421e976d6d7ab1b39bec",
    name: "Device 1",
    mac: "",
    PM25: 50,
    PM1: 20,
    PM10: 50,
    CO2: 900,
    TVOC: 0.5,
    HCHO: 0.1,
    SLOPE: 0,
    ssid: "",
    systemTick: 0,
    lastUpdate: new Date().toISOString(),
    createdAt: new Date(),
    updatedAt: new Date()
}, {
    devId: "D1-657373a1e14ef08b3dc854a76b90adb6",
    name: "Device 2",
    mac: "",
    PM25: 10,
    PM1: 2,
    PM10: 30,
    CO2: 500,
    TVOC: 0.15,
    HCHO: 0.3,
    SLOPE: 0,
    ssid: "",
    systemTick: 0,
    lastUpdate: new Date().toISOString(),
    createdAt: new Date(),
    updatedAt: new Date()
}, {
    devId: "D1-bbabca75c1b0421e976d6d7ab1b39bec",
    name: "Device 1",
    mac: "",
    PM25: 200,
    PM1: 140,
    PM10: 300,
    CO2: 1600,
    TVOC: 0.5,
    HCHO: 0.6,
    SLOPE: 0,
    ssid: "",
    systemTick: 0,
    lastUpdate: new Date().toISOString(),
    createdAt: new Date(),
    updatedAt: new Date()
}, {
    devId: "D1-657373a1e14ef08b3dc854a76b90adb6",
    name: "Device 2",
    mac: "",
    PM25: 100,
    PM1: 4,
    PM10: 160,
    CO2: 300,
    TVOC: 0.5,
    HCHO: 0.95,
    SLOPE: 0,
    ssid: "",
    systemTick: 0,
    lastUpdate: new Date().toISOString(),
    createdAt: new Date(),
    updatedAt: new Date()
}]

function randomGenerator(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function randDocGenerator() {
    const ids = ["D1-bbabca75c1b0421e976d6d7ab1b39bec", "D1-657373a1e14ef08b3dc854a76b90adb6"];
    const index = randomGenerator(0, 1);
    const object = {
        devId: ids[index],
        name: `Device ${index + 1}`,
        mac: "",
        PM25: randomGenerator(5, 200),
        PM1: randomGenerator(5, 100),
        PM10: randomGenerator(5, 400),
        CO2: randomGenerator(50, 2000),
        TVOC: randomGenerator(1, 10),
        HCHO: randomGenerator(1, 10),
        SLOPE: 0,
        ssid: "",
        systemTick: 0,
        lastUpdate: new Date().toISOString(),
        createdAt: new Date(),
        updatedAt: new Date()
    }
    return object;
}

function addNewReadings() {
    readings.insertMany([randDocGenerator()], (err, doc) => {
        if (err) throw err;
        else console.log(doc);
    });
}

function getReadings() {
    return readings.aggregate([
        {
            $match: {
                createdAt: {
                    $gt: moment().subtract(30, 'seconds').utc().toDate()
                }
            }
        }, {
            $group: {
                _id: "$devId",
                sumPM25: { $sum: '$PM25' },
                countPM25: { $sum: 1 },
                sumPM1: { $sum: '$PM1' },
                countPM1: { $sum: 1 },
                sumPM10: { $sum: '$PM10' },
                countPM10: { $sum: 1 },
                sumCO2: { $sum: '$CO2' },
                countCO2: { $sum: 1 },
                sumTVOC: { $sum: '$TVOC' },
                countTVOC: { $sum: 1 },
                sumHCHO: { $sum: '$HCHO' },
                countHCHO: { $sum: 1 },
                recentData: { "$last": "$$ROOT" }
            }
        }, {
            $project: {
                averagePM25: { $divide: ["$sumPM25", "$countPM25"] },
                averagePM1: { $divide: ["$sumPM1", "$countPM1"] },
                averagePM10: { $divide: ["$sumPM10", "$countPM10"] },
                averageCO2: { $divide: ["$sumCO2", "$countCO2"] },
                averageTVOC: { $divide: ["$sumTVOC", "$countTVOC"] },
                averageHCHO: { $divide : [{ $divide: ["$sumHCHO", "$countHCHO"] }, 1000]},
                recentData: 1
            }
        }
    ]);
}


module.exports = { addNewReadings, getReadings };
