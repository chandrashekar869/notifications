const Alerts = require('./alerts.model');

function insert(params, cb) {
    const alert = new Alerts(params);
    return alert.save();
}

function get(conditions) {
    return Alerts.find(conditions);
}

module.exports = {
    insert, 
    get
}