let mongoose = require('mongoose');
const moment = require('moment');
let Schema = mongoose.Schema;
let ObjectId = mongoose.Schema.Types.ObjectId;
// Schema definition
let AlertsSchema = new Schema(
  {
    devId: { type: String },
    pollutant: { type: String },
    status: { type: Boolean },
    max: { type: Number },
    min: { type: Number },
    updated: { type: Boolean },
    userId: { type: ObjectId },
    threshold: { type: Number },
    createdAt: { type: Date },
    updatedAt: { type: Date, default: moment.utc().toDate() },
    originalData: { type: Object, required: true },
    recentData: { type: Object, required: false }
  }
);
// Sets the createdAt parameter equal to the current time
// ReadingsSchema.pre('save', next => {
//     now = new Date();
//     if(!this.createdAt) {
//       this.createdAt = now;
//     }
//     next();
//   });

// Exports the Schema for use elsewhere.
module.exports = mongoose.model('alerts', AlertsSchema);